//
//  MMGooglePlacesExampleTests.swift
//  MMGooglePlacesExampleTests
//
//  Created by Madhura Marathe on 14/07/17.
//  Copyright © 2017 Madhura Marathe. All rights reserved.
//

import XCTest
@testable import MMGooglePlacesExample
@testable import Alamofire
@testable import SwiftyJSON


class MMGooglePlacesExampleTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testAsyncRequest() {
        let asyncexpectation = expectation(description: "Alamofire")
        
        // search for "Pune"
        let urlString = "https://maps.googleapis.com/maps/api/place/autocomplete/json?key=AIzaSyCvZ0PNGHEP-w_Jwg-QdgeEDt-i1HmKB7k&input=pune"
        
        Alamofire.request(urlString).response { response in
            
            Alamofire.request(urlString).responseJSON { (responseData) -> Void in
                if responseData.result.value != nil {
                    switch responseData.result {
                    case .success:
                        let jsonVar = JSON(responseData.result.value!)
                        if jsonVar != JSON.null {
                            XCTAssertTrue(true, "Status code 200. Success")
                            asyncexpectation.fulfill()
                        } else {
                            XCTAssert(false, "Data is nil")
                        }
                    case .failure(let error):
                        XCTAssertNil(error, "Error:\(error)")
                    }
                } else {
                    XCTAssert(false, "System error")
                }
            }
        }
        
        //wai(5.0, handler: nil)
        wait(for: [asyncexpectation], timeout: 10.0)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
