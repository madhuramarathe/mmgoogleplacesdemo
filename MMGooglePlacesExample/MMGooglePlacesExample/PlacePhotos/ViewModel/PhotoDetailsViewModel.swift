//
//  PhotoDetailsViewModel.swift
//  MMGooglePlacesExample
//
//  Created by Madhura Marathe on 7/17/17.
//  Copyright © 2017 Madhura Marathe. All rights reserved.
//

import UIKit
import SwiftyJSON

protocol PhotoDetailsViewModelProtocol: class {
    func showImages(images: [UIImage])
    func showError(errorMessage: String)
    func showAlert(alertMessage: String)
}

class PhotoDetailsViewModel {
    // MARK: - Photo Variables
    let group = DispatchGroup()
    var photos = [UIImage]()
    var photosUrl = [URL]()
    var photoNames = [String]()
    
    weak var photoViewModelProtocolDelegate: PhotoDetailsViewModelProtocol?
    // place nearby places URL
    private static let nearbyPlacesUrl = "place/nearbysearch/json?"
    // place photo URL
    private static let photoUrl = "place/photo?maxwidth=400&photoreference="
    
    // MARK: - Private methods
    private func getNearbyPlacesUrl() -> String {
        return WebserviceConstants.baseUrl + PhotoDetailsViewModel.nearbyPlacesUrl + "key=\((UIApplication.shared.delegate as! AppDelegate).getApiKey())"
    }
    
    private func getPhotoDetailsUrl() -> String {
        return WebserviceConstants.baseUrl + PhotoDetailsViewModel.photoUrl
    }
    
    /*
     gets place details
     parameters:
     1) placeid : String
     */
    func getPhotoDetails(_ lat: Double,long: Double, radius: Int,name: String) {
        let urlString = getNearbyPlacesUrl() + "&location=\(lat),\(long)&radius=\(radius)"
        
        HttpCommunication.sharedInstance.fetch(requestUrlStr:urlString,
                                               responseHandler: {[weak self] (response: JSON) in
                                                self?.parseNearbyPlacesJson(response: response)
                                                self?.group.notify(queue: DispatchQueue.global(qos: .background)) {
                                                    self?.photoViewModelProtocolDelegate?.showImages(images: (self?.photos)!)
                                                }
                                                
            },
                                               errorHandler: {[weak self] (error: Error) in
                                                self?.handleError(error:error)
                                                
            })
    }
    
    /* Parse Json response */
    func parseNearbyPlacesJson(response : JSON) {
        let photoDetail = PhotoDetails()
        
        let results = response["results"]
        
        
        for (_,result) in results {
            let name  = result["name"]
            photoNames.append(name.stringValue)
            let photos = result["photos"]
            
            for (_,photo) in photos {
                // fetch photo refernce for each place to form url 
                if let photo_reference = photo["photo_reference"].string {
                    photoDetail.photoReference = photo_reference
                    // form photoUrl
                    addPhotoUrl(reference: photoDetail.photoReference)
                }
            }
        }
        
        // download images
        
        for url in photosUrl {
            group.enter()
            DispatchQueue.global().async {
                let data = try? Data(contentsOf: url) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                self.photos.append(UIImage(data: data!)!)
                self.group.leave()
            }
        }
    }
    
    func addPhotoUrl(reference : String) {
        let urlstring = getPhotoDetailsUrl() + reference + "&key=\((UIApplication.shared.delegate as! AppDelegate).getApiKey())"
        let url = URL(string: urlstring)
        photosUrl.append(url!)
    }

    
    // MARK: - Error Handling
    func handleError(error: Error) {
        var errorMessage = ""
        switch(error) {
        case NetworkError.NoDataError:
            errorMessage = NSLocalizedString("No Datafound", comment:"No Data found. Please check connection or try again later.")
            
        case NetworkError.JSONSerializationError:
            errorMessage =  NSLocalizedString("Something went wrong during JsonSerialization", comment:"Something went wrong during JsonSerialization")
            
        case NetworkError.ServerError:
            errorMessage =  NSLocalizedString("Error fetching data from the Server", comment:"Error fetching data from the Server")
            
        default :
            errorMessage = error.localizedDescription
        }
        self.photoViewModelProtocolDelegate?.showError(errorMessage: errorMessage)
    }
    
    // MARK: - Save Image
    /* Save downloaded image to the Documents Directory */
    func save(image: UIImage, index : Int) {
        if let data = UIImagePNGRepresentation(image) {
            let name = photoNames[index]
            let filename = getDocumentsDirectory().appendingPathComponent("\(name).png")
            let fileManager = FileManager.default
            let filePath = filename.path
            if fileManager.fileExists(atPath: filePath) {
                photoViewModelProtocolDelegate?.showAlert(alertMessage: NSLocalizedString("Image is already saved to the Documents Directory",comment:"Image is already saved to the Documents Directory"))
            } else {
                try? data.write(to: filename)
                photoViewModelProtocolDelegate?.showAlert(alertMessage: NSLocalizedString("Your image is saved to the Documents Directory",comment:"Your image is saved to the Documents Directory"))
            }
            
        }
    }
    
    /* Get documents directory path */
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
}
