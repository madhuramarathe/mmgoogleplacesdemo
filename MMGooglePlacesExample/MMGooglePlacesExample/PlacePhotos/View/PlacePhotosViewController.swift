//
//  PlacePhotosViewController.swift
//  MMGooglePlacesExample
//
//  Created by Madhura Marathe on 17/07/17.
//  Copyright © 2017 Madhura Marathe. All rights reserved.
//

import UIKit

class PlacePhotosViewController: UIViewController {

    // MARK: IBOutlets
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var noImagesLabel: UILabel!
    // MARK: Variables
    var nearbyPhotos = [UIImage]()
    var photoDetailViewModel : PhotoDetailsViewModel!
    var selectedImage : UIImage!
    
    // MARK: Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        photoDetailViewModel = PhotoDetailsViewModel()
        photoDetailViewModel.photoViewModelProtocolDelegate = self
    }
    
    func setDetails(lat: Double, long: Double, name: String) {
        if let detailViewModel = self.photoDetailViewModel {
            activityIndicator.startAnimating()
            detailViewModel.getPhotoDetails(lat, long: long, radius: 500, name: name)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == GUIConstants.SegueIdentifiers.FullScreenPhoto,
            let fullScreenVC = segue.destination as? FullScreenPhotoViewer {
            
            if let image = selectedImage {
                fullScreenVC.setImage(image: image)
            }
        }
    }
}

extension PlacePhotosViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return nearbyPhotos.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: GUIConstants.CellIdentifiers.PhotoCell, for: indexPath as IndexPath) as! PhotoCollectionViewCell

        cell.photoImageView.image = nearbyPhotos[indexPath.row]
        return cell
    }

}

extension PlacePhotosViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedImage = nearbyPhotos[indexPath.row]
        photoDetailViewModel.save(image: selectedImage, index : indexPath.row)
    }
}

extension PlacePhotosViewController: PhotoDetailsViewModelProtocol {
    
    // show lat long of the selected place on the map
    func showImages(images: [UIImage]) {
        nearbyPhotos = images
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
        
            if self.nearbyPhotos.isEmpty {
                self.noImagesLabel.isHidden = false
                self.collectionView.isHidden = true
            
            } else {
                self.noImagesLabel.isHidden = true
                self.collectionView.isHidden = false
                self.collectionView.reloadData()
            }
        }
    }
    
    // error fetching predictions. show alert
    func showAlert(alertMessage: String) {
        let alert = UIAlertController(title: NSLocalizedString("Hey", comment: "Hey"),
                                      message: alertMessage,
                                      preferredStyle: UIAlertControllerStyle.alert)
        
        let dismiss = UIAlertAction(title: NSLocalizedString("OK", comment: "OK"), style: .cancel, handler: {(alert: UIAlertAction!) in
            self.performSegue(withIdentifier: GUIConstants.SegueIdentifiers.FullScreenPhoto, sender: self)
        })
        alert.addAction(dismiss)
        self.present(alert, animated: true, completion: nil)
    }
    
    func showError(errorMessage: String) {
        let alert = UIAlertController(title: NSLocalizedString("Error", comment: "Error"),
                                      message: errorMessage,
                                      preferredStyle: UIAlertControllerStyle.alert)
        
        let dismiss = UIAlertAction(title: NSLocalizedString("OK", comment: "OK"), style: .cancel, handler: nil)
        alert.addAction(dismiss)
        self.present(alert, animated: true, completion: nil)
    }
}
