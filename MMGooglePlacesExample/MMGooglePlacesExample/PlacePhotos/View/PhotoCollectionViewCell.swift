//
//  PhotoCollectionViewCell.swift
//  MMGooglePlacesExample
//
//  Created by Madhura Marathe on 17/07/17.
//  Copyright © 2017 Madhura Marathe. All rights reserved.
//

import UIKit

class PhotoCollectionViewCell: UICollectionViewCell {
    
    // MARK: - IBOutlets
    @IBOutlet weak var photoImageView: UIImageView!
}
