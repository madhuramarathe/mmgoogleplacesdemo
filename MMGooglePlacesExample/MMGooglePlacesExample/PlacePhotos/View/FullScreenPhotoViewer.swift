//
//  FullScreenPhotoViewer.swift
//  MMGooglePlacesExample
//
//  Created by Madhura Marathe on 18/07/17.
//  Copyright © 2017 Madhura Marathe. All rights reserved.
//

import UIKit

class FullScreenPhotoViewer: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet weak var photoImageView: UIImageView! {
        didSet {
            photoImageView.image = selectedImage
            photoImageView.contentMode = .scaleAspectFit
        }
    }
    
    // MARK: - Variables
    var selectedImage : UIImage!
    
    // MARK: - Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    func setImage(image: UIImage) {
        selectedImage = image
    }
}
