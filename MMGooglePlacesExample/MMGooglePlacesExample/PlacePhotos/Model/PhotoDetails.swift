//
//  PhotoDetails.swift
//  MMGooglePlacesExample
//
//  Created by Madhura Marathe on 17/07/17.
//  Copyright © 2017 Madhura Marathe. All rights reserved.
//

import UIKit
import SwiftyJSON

class PhotoDetails {
    
    /* Photo Reference */
    var photoReference = ""
    
    /* photo List */
    var photosList = [JSON]()
}
