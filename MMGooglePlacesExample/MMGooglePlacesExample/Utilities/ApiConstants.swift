//
//  ApiConstants.swift
//  MMGooglePlacesExample
//
//  Created by Damle, Neeraj on 7/16/17.
//  Copyright © 2017 Madhura Marathe. All rights reserved.
//

import Foundation


struct WebserviceConstants {
    // base URL
    static let baseUrl = "https://maps.googleapis.com/maps/api/"
}
