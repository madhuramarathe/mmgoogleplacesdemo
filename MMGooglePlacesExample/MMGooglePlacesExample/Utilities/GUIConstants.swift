//
//  GUIConstants.swift
//  MMGooglePlacesExample
//
//  Created by Madhura Marathe on 14/07/17.
//  Copyright © 2017 Madhura Marathe. All rights reserved.
//

import Foundation

struct GUIConstants {
    
    struct StoryboardIds {
        static let SearchPlacesViewController = "SearchPlacesViewController"
        static let PlaceDetailsViewController = "PlaceDetailsViewController"
        static let PlacePhotosViewController = "PlacePhotosViewController"
    }
    
    struct SegueIdentifiers {
        static let SearchToPlaceDetails = "SearchToPlaceDetails"
        static let MapView = "MapView"
        static let PhotoView = "PhotoView"
        static let FullScreenPhoto = "FullScreenPhoto"
    }
    
    struct CellIdentifiers {
        static let PhotoCell = "PhotoCell"
        static let SearchCell = "Cell"
    }
}
