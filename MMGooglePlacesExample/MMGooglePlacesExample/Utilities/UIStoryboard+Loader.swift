//
//  File.swift
//  MMGooglePlacesExample
//
//  Created by Madhura Marathe on 14/07/17.
//  Copyright © 2017 Madhura Marathe. All rights reserved.
//

import UIKit

fileprivate enum Storyboard : String {
    case main = "Main"
}

fileprivate extension UIStoryboard {
    
    static func loadFromMain(_ identifier: String) -> UIViewController {
        return load(from: .main, identifier: identifier)
    }
    
    static func load(from storyboard: Storyboard, identifier: String) -> UIViewController {
        let uiStoryboard = UIStoryboard(name: storyboard.rawValue, bundle: nil)
        return uiStoryboard.instantiateViewController(withIdentifier: identifier)
    }
}

// MARK: App View Controllers
extension UIStoryboard {
    static func loadSearchPlacesViewController() -> SearchPlacesViewController {
        return loadFromMain(GUIConstants.StoryboardIds.SearchPlacesViewController) as! SearchPlacesViewController
    }
    
    static func loadPlaceDetailsViewController() -> PlaceDetailsViewController {
        return loadFromMain(GUIConstants.StoryboardIds.PlaceDetailsViewController) as! PlaceDetailsViewController
    }
    
    static func loadPlacePhotosViewController() -> PlacePhotosViewController {
        return loadFromMain(GUIConstants.StoryboardIds.PlacePhotosViewController) as! PlacePhotosViewController
    }
}

