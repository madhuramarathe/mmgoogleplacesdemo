//
//  PlaceAndPhotoDetailViewController.swift
//  MMGooglePlacesExample
//
//  Created by Madhura Marathe on 17/07/17.
//  Copyright © 2017 Madhura Marathe. All rights reserved.
//

import UIKit

class PlaceAndPhotoDetailViewController: UIViewController,PlaceDetailsViewControllerProtocol {
    
    // MARK: Variables
    var selectedPlace : SearchPlace!
    private var photoDetailVC : PlacePhotosViewController!
    
    // MARK: Functions
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == GUIConstants.SegueIdentifiers.MapView,
            let placeDetails = segue.destination as? PlaceDetailsViewController {
            placeDetails.selectedPlace = self.selectedPlace
            placeDetails.placeViewProtocolDelegate = self
            
        } else if segue.identifier == GUIConstants.SegueIdentifiers.PhotoView {
            photoDetailVC = segue.destination as? PlacePhotosViewController
        }
    }
    
    // MARK: - PlaceDetailsViewControllerProtocol
    func locationReceived(lat: Double, long: Double, name: String) {
        photoDetailVC.setDetails(lat: lat, long: long, name: name)
    }
}
