//
//  PlaceDetailsViewController.swift
//  MMGooglePlacesExample
//
//  Created by Madhura Marathe on 14/07/17.
//  Copyright © 2017 Madhura Marathe. All rights reserved.
//

import UIKit
import GoogleMaps

protocol PlaceDetailsViewControllerProtocol: class {
    func locationReceived(lat: Double, long: Double, name: String)
}

class PlaceDetailsViewController: UIViewController {

    // MARK: IBOutlets
    @IBOutlet weak var mapView: UIView!
    
    // MARK: Variables
    weak var placeViewProtocolDelegate: PlaceDetailsViewControllerProtocol?
    var selectedPlace : SearchPlace!
    var placeDetailViewModel : PlaceDetailsViewModel!
    // mapview
    var googleMap = GMSMapView()
    // annotation/marker
    let marker = GMSMarker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
    }
    
    func setUpUI() {
        // create view model
        placeDetailViewModel = PlaceDetailsViewModel()
        placeDetailViewModel.viewModelProtocolDelegate = self
        // fetch place details
        placeDetailViewModel.getPlaceDetails(selectedPlace.placeId)
    }
}

extension PlaceDetailsViewController : PlaceDetailsViewModelProtocol {
    
    // show lat long of the selected place on the map
    func showLocationOnMap(lat: Double, long: Double, name: String) {
        
        placeViewProtocolDelegate?.locationReceived(lat: lat, long: long, name: name)
        
        googleMap = GMSMapView()
        
        googleMap.camera = GMSCameraPosition(target: CLLocationCoordinate2D(latitude:lat, longitude: long), zoom: 10.0, bearing: 0, viewingAngle : 0)

        googleMap.layer.cameraLatitude = lat
        googleMap.layer.cameraLongitude = long
        
        view = googleMap
        
        // Create marker and set location
        marker.position = CLLocationCoordinate2DMake(lat, long)
        marker.title = name
        marker.map = self.googleMap
    }
    
    // error fetching predictions. show alert
    func showError(errorMessage: String) {
        let alert = UIAlertController(title: NSLocalizedString("Error", comment: "Error"),
                                      message: errorMessage,
                                      preferredStyle: UIAlertControllerStyle.alert)
        
        let dismiss = UIAlertAction(title: NSLocalizedString("OK", comment: "OK"), style: .cancel, handler: nil)
        alert.addAction(dismiss)
        self.present(alert, animated: true, completion: nil)
    }
}
