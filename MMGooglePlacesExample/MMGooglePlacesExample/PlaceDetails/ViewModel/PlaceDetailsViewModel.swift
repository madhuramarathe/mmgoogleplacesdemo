//
//  PlaceDetailsViewModel.swift
//  MMGooglePlacesExample
//
//  Created by Madhura Marathe on 7/16/17.
//  Copyright © 2017 Madhura Marathe. All rights reserved.
//

import UIKit
import SwiftyJSON

protocol PlaceDetailsViewModelProtocol: class {
    func showLocationOnMap(lat: Double, long: Double, name: String)
    func showError(errorMessage: String)
}

class PlaceDetailsViewModel: NSObject {
    
    // MARK: - Place Details Variables
    weak var viewModelProtocolDelegate: PlaceDetailsViewModelProtocol?
    
    // place detail URL
    private static let placeDetailsUrl = "place/details/json?"
    
    // MARK: Functions
    private func getPlaceDetailsUrl() -> String {
        return WebserviceConstants.baseUrl + PlaceDetailsViewModel.placeDetailsUrl + "key=\((UIApplication.shared.delegate as! AppDelegate).getApiKey())"
    }
    
    /*
     gets place details
     parameters:
     1) placeid : String
     */
    func getPlaceDetails(_ placeId: String) {
        let urlString = getPlaceDetailsUrl() + "&placeid=\(placeId)"
        HttpCommunication.sharedInstance.fetch(requestUrlStr:urlString,
                                               responseHandler: {[weak self] (response: JSON) in
                                                let placeDetail = self?.parsePlaceDetailsJson(response: response)
                                                
                                                if let detail = placeDetail {
                                                    
                                                    // ask UI to render
                                                self?.viewModelProtocolDelegate?.showLocationOnMap(lat: detail.lat, long: detail.long, name: detail.name)
                                                }
                                                
            },
                                               errorHandler: {[weak self] (error: Error) in
                                                
                                                self?.handleError(error:error)
            })
    }
    
    func parsePlaceDetailsJson(response : JSON) -> PlaceDetail {
 
        let placeDetail = PlaceDetail()
        
        if let id = response["result"]["id"].string {
            placeDetail.id = id
        }
        
        if let placeid = response["result"]["place_id"].string {
            placeDetail.placeid = placeid
        }
        
        if let name = response["result"]["name"].string {
            placeDetail.name = name
        }
        
        if let addressComponents = response["result"]["address_components"].array {
            placeDetail.addressComponents = addressComponents
        }
        
        if let icon = response["result"]["icon"].string {
            placeDetail.icon = icon
        }
        
        if let geometry = response["result"]["geometry"].dictionary {
            if let location = geometry["location"]?.dictionary {
                if let lat = location["lat"]?.doubleValue, let long = location["lng"]?.doubleValue {
                    placeDetail.lat = lat
                    placeDetail.long = long
                }
            }
        }
        return placeDetail
    }
    
    // MARK: - Error Handling
    func handleError(error: Error) {
        var errorMessage = ""
        switch(error) {
        case NetworkError.NoDataError:
            errorMessage = NSLocalizedString("No Datafound", comment:"No Data found. Please check connection or try again later.")
            
        case NetworkError.JSONSerializationError:
            errorMessage =  NSLocalizedString("Something went wrong during JsonSerialization", comment:"Something went wrong during JsonSerialization")
            
        case NetworkError.ServerError:
            errorMessage =  NSLocalizedString("Error fetching data from the Server", comment:"Error fetching data from the Server")
            
        default :
            errorMessage = error.localizedDescription
        }
        self.viewModelProtocolDelegate?.showError(errorMessage: errorMessage)
    }
}







