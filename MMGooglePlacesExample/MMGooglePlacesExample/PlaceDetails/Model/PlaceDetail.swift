//
//  PlaceDetail.swift
//  MMGooglePlacesExample
//
//  Created by Madhura Marathe on 7/16/17.
//  Copyright © 2017 Madhura Marathe. All rights reserved.
//

import SwiftyJSON
import UIKit

class PlaceDetail {
    /* id */
    var id = ""
    
    /* addressComponents */
    var addressComponents = [JSON]()
    
    /* vicinity */
    var vicinity = ""
    
    /* formattedAddress */
    var formattedAddress = ""
    
    /* lat */
    var lat = 0.0
    
    /* long */
    var long = 0.0
    
    /* icon */
    var icon = ""
    
    /* placeid */
    var placeid = ""
    
    /* name */
    var name = ""
}
