//
//  SearchPlacesViewController.swift
//  MMGooglePlacesExample
//
//  Created by Madhura Marathe on 14/07/17.
//  Copyright © 2017 Madhura Marathe. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class SearchPlacesViewController: UIViewController {
    
    // MARK: IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: Variables
    fileprivate var searchArray = [SearchPlace]()
    fileprivate var searchPlaceViewModel : SearchPlacesViewModel!
    
    fileprivate var nameSearchController: UISearchController = ({
        /* Display search results in a separate view controller */
        
        let controller = UISearchController(searchResultsController: nil)
        controller.hidesNavigationBarDuringPresentation = false
        controller.dimsBackgroundDuringPresentation = false
        controller.searchBar.searchBarStyle = .minimal
        controller.searchBar.sizeToFit()
        return controller
    })()
    
    // MARK: Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.topItem?.title = NSLocalizedString("Search Place", comment: "Search Place")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.navigationBar.topItem?.title = " "
    }


    func setUpUI() {
        self.definesPresentationContext = true
        // Configure countryTable
        tableView.tableHeaderView = nameSearchController.searchBar
        nameSearchController.searchResultsUpdater = self
        
        // create ViewModel
        searchPlaceViewModel = SearchPlacesViewModel()
        searchPlaceViewModel.viewModelProtocolDelegate = self
    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == GUIConstants.SegueIdentifiers.SearchToPlaceDetails,
            let placeAndPhotoDetails = segue.destination as? PlaceAndPhotoDetailViewController,
            let indexPath = self.tableView.indexPathForSelectedRow {
            let selectedPlace = searchArray[indexPath.row]
            placeAndPhotoDetails.selectedPlace = selectedPlace
        }
    }
 }

extension SearchPlacesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: GUIConstants.CellIdentifiers.SearchCell)!
        cell.textLabel?.text = searchArray[indexPath.row].description
        return cell
    }
}

extension SearchPlacesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension SearchPlacesViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        if searchController.searchBar.text?.utf8.count == 0 {
            searchArray.removeAll()
            tableView.reloadData()
        } else {
            // call api to fetch auto complete suggestions
            if let text = searchController.searchBar.text {
                searchPlaceViewModel.searchPlace(text)
            }
        }
    }
}

extension SearchPlacesViewController : SearchPlacesViewModelProtocol {
    // received search Names. Show in tableview
    func showSearchNames(predictions : [SearchPlace]) {
        searchArray.removeAll()
        searchArray = predictions
        tableView.reloadData()
    }
    
    // error fetching predictions. show alert
    func showError(errorMessage : String) {
        let alert = UIAlertController(title: NSLocalizedString("Error", comment: "Error"),
                                      message: errorMessage,
                                      preferredStyle: UIAlertControllerStyle.alert)
        
        let dismiss = UIAlertAction(title: NSLocalizedString("OK", comment: "OK"), style: .cancel, handler: nil)
        alert.addAction(dismiss)
        self.present(alert, animated: true, completion: nil)
    }
}


