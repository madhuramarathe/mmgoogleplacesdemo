//
//  SearchPlacesViewModel.swift
//  MMGooglePlacesExample
//
//  Created by Madhura Marathe on 14/07/17.
//  Copyright © 2017 Madhura Marathe. All rights reserved
//

import UIKit
import SwiftyJSON

protocol SearchPlacesViewModelProtocol : class {
    func showSearchNames(predictions : [SearchPlace])
    func showError(errorMessage : String)
}

class SearchPlacesViewModel {
    // MARK: - Variables
    weak var viewModelProtocolDelegate: SearchPlacesViewModelProtocol?
    // search Url URL
    private static let searchUrl = "place/autocomplete/json?"
    
    var predictionList : [SearchPlace]?
    
    // MARK: Functions
    private func getSearchUrl() -> String {
        return WebserviceConstants.baseUrl + SearchPlacesViewModel.searchUrl + "key=\((UIApplication.shared.delegate as! AppDelegate).getApiKey())"
    }
    
    /*
     searches name 
     parameters:
     1) placeName : String
     */
    func searchPlace(_ name: String) {
        // remove all objects for new fetch request
        predictionList?.removeAll()
        
        if predictionList == nil {
            predictionList = [SearchPlace]()
        }
        
        let searchString = getSearchUrl() + "&input=\(name)"
        HttpCommunication.sharedInstance.fetch(requestUrlStr:searchString,
        responseHandler: {[weak self] (response: JSON) in
            self?.parseJson(response: response)
            if let predictions = self?.predictionList {
                self?.viewModelProtocolDelegate?.showSearchNames(predictions: predictions)
            } else {
                let errorMessage = NSLocalizedString("No Places found.", comment:"No Places found.")
                self?.viewModelProtocolDelegate?.showError(errorMessage: errorMessage)
            }
        },
        errorHandler: {[weak self] (error: Error) in
            
            self?.handleError(error:error)
        })
    }
    
    func parseJson(response : JSON) {
        for prediction in response["predictions"].arrayValue {
            let searchPrediction = SearchPlace()
            
            if let pid = prediction.dictionary?["id"]?.stringValue {
                searchPrediction.predictionID = pid
            }
            
            if let placeId = prediction.dictionary?["place_id"]?.stringValue {
                searchPrediction.placeId = placeId
            }
            
            if let desc = prediction.dictionary?["description"]?.stringValue {
                searchPrediction.description = desc
            }
            predictionList?.append(searchPrediction)
        }
    }
    
    // MARK: - Error Handling
    func handleError(error: Error) {
        var errorMessage = ""
        switch(error) {
        case NetworkError.NoDataError:
            errorMessage = NSLocalizedString("No Datafound", comment:"No Data found. Please check connection or try again later.")
            
        case NetworkError.JSONSerializationError:
            errorMessage =  NSLocalizedString("Something went wrong during JsonSerialization", comment:"Something went wrong during JsonSerialization")
            
        case NetworkError.ServerError:
            errorMessage =  NSLocalizedString("Error fetching data from the Server", comment:"Error fetching data from the Server")
            
        default :
            errorMessage = error.localizedDescription
        }
        self.viewModelProtocolDelegate?.showError(errorMessage: errorMessage)
    }
}
