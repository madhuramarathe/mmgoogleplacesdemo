//
//  SearchPlace.swift
//  MMGooglePlacesExample
//
//  Created by Damle, Neeraj on 7/16/17.
//  Copyright © 2017 Madhura Marathe. All rights reserved.
//

import UIKit
// Only required fields are parsed and saved in the model. Entire Json is not parsed as its not needed
class SearchPlace {
    
    /* prediction ID : String */
    var predictionID = ""
    
    /*Types array of strings*/
    var types = [String]()
    
    /*Place ID*/
    var placeId = ""
    
    /* Description */
    var description = ""
    
    /* Structured Formatting */
    var structuredFormatting = [String:Any]()
    
    /* Terms */
    var terms = [[String:Any]]()
}
