//
//  HttpCommunication.swift
//  MMGooglePlacesExample
//
//  Created by Madhura Marathe on 14/07/17.
//  Copyright © 2017 Madhura Marathe. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

enum HTTP_METHOD : String {
    case GET = "GET"
    case POST = "POST"
    case PUT = "PUT"
}

public enum NetworkError : Error {
    case ConnectionError(error: NSError)
    // The server responded with a non 200 status code
    case ServerError
    // We got no data (0 bytes) back from the server
    case NoDataError
    // The server response can't be converted from JSON to a Dictionary
    case JSONSerializationError
}

class HttpCommunication: NSObject {
    
    static let sharedInstance = HttpCommunication()
  
    // MARK: Functions
    func fetch(requestUrlStr : String, responseHandler : @escaping (_ response: JSON)->Void, errorHandler : @escaping (_ error: Error) -> Void) {
      
        print("\(requestUrlStr)")
      Alamofire.request(requestUrlStr).responseJSON { (responseData) -> Void in
        if responseData.result.value != nil {
            switch responseData.result {
            case .success:
                let jsonVar = JSON(responseData.result.value!)
                if jsonVar != JSON.null {
                    print(jsonVar)
                    responseHandler (jsonVar)
                } else {
                    errorHandler(NetworkError.JSONSerializationError)
                }
            case .failure(let error):
                if error.localizedDescription.isEmpty {
                    errorHandler(NetworkError.ServerError)
                } else {
                    errorHandler(error)
                }
            }
        } else {
            errorHandler(NetworkError.NoDataError)
        }
      }
    }
}
