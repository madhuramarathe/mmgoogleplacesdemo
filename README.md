# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
This repository is the demo app for the Google Places API.

Following is the Workflow:

1) Search Screen : 
User enters the place name to search and search results are poplulated in the list below.
On click of any list item from the search results, Place Details screen is displayed. 

2) Place Details Screen : 
It displays a map of the place entered by the user on previous screen. 
It also displays the thumbnail photos related to the place at the bottom of the screen in horizontal manner. 
On click of any photo, that photo is saved in app's document directory.

3) Save Photo and View Photo in full screen mode :
As the photo gets saved in the documents directory, an alert is shown to notify the user with OK button. 
On click of OK, the image gets opened in the Full Screen View on the next Screen.


### How do I get set up? ###
The project needs cocoapods installation.
Reference Link: https://guides.cocoapods.org/using/getting-started.html
Once cocoapods is installed, go to the project directory and type pod install to fetch the libraries used in the project.

### Following libraries used: ###
1) Alamofire for Http Communication.
2) SwiftyJSON for json serialization.
3) GoogleMaps.
4) GooglePlaces.
